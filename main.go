package main

import (
	"flag"
	"fmt"
	"time"
)

/*
  Current state:

  Issue: Leap seconds
  -------------------
  The further in time we progress, the less accurate this becomes.
  It is most accurate when using the 4000 BC julian time, since less
  leap seconds have occurred.

  Refactor: The `time` package should be used much more often
  -----------------------------------------------------------
  For example, time.LoadLocation("America/Los_Angeles") could be used
  to return a location. This location can be used to convert UTC to a
  specific time. https://pkg.go.dev/time#LoadLocation

  Nice to have: Automatic documentation from comments
  ---------------------------------------------------
  Golang has the ability to generate docs directly from comments.

  Refactor: Use a separate function for formatting
  ------------------------------------------------
  Currently calcSunriseSet does all the formatting. Instead, have
  this function return time as a unix timestamp and convert it
  using other helper functions.
*/

func main() {
	n := time.Now()
	l, _ := time.LoadLocation("Local")
	_, o := n.In(l).Zone()
	o = o / 60 / 60
	t := float64(o)

	// Longitude/Latitude
	lat := flag.Float64("lat", 0.0, "Default: 0.0; Latitude")
	long := flag.Float64("long", 0.0, "Default: 0.0; Longitude")

	// Timezone
	tz := flag.Float64("tz", t, fmt.Sprintf("Default: %d; Timezone used to calculate sun(rise|set). Uses local timezone if no timezone is specified.", int(t)))

	// Daylight savings time
	dst := flag.Bool("dst", false, "Default: false; True to calculate sun(rise|set) relative to daylight savings time")

	// Sunrise or Sunset?
	rise := flag.Bool("rise", true, "Default: true; True to return sunrise, false to return sunset")

	// Time
	ts := flag.Float64("time", float64(n.Unix()), "Default: Now as unix timestamp; Time formatted as a Unix timestamp")
	isJulian := flag.Bool("isJulian", false, "Default: false; Specify true when the time argument is a julian timestamp")

	// Formats
	unixTimestamp := flag.Bool("u", false, "Default: true; True to output as a unix timestamp")

	flag.Parse()

	// Convert time to julian time
	var julian float64
	if *isJulian == false {
		julian = getJulianFromUnixTimestamp(*ts)
	} else {
		julian = *ts
	}

	fmt.Println(calcSunriseSet(*rise, julian, *lat, *long, *tz, *dst, *unixTimestamp))
}
