package main

import (
	"fmt"
	"math"
	"strconv"
	"time"
	"unicode/utf8"
)

func calcTimeJulianCent(jd float64) float64 {
	return (jd - 2451545.0) / 36525.0
}

func calcJDFromJulianCent(t int) int {
	return t*36525.0 + 2451545.0
}

func isLeapYear(yr int) bool {
	return ((yr%4 == 0 && yr%100 != 0) || yr%400 == 0)
}

func radToDeg(angleRad float64) float64 {
	return (180.0 * angleRad / math.Pi)
}

func degToRad(angleDeg float64) float64 {
	return (math.Pi * angleDeg / 180.0)
}

// Return degrees
func calcGeomMeanLongSun(t float64) float64 {
	L0 := 280.46646 + t*(36000.76983+t*(0.0003032))
	for L0 > 360 {
		L0 -= 360.0
	}
	for L0 < 0.0 {
		L0 += 360.0
	}
	return L0
}

// Return degrees
func calcGeomMeanAnomalySun(t float64) float64 {
	return 357.52911 + t*(35999.05029-0.0001537*t)
}

// From https://en.wikipedia.org/wiki/Orbital_eccentricity:
// In astrodynamics, the orbital eccentricity of an astronomical object
// is a dimensionless parameter that determines the amount by which its
// orbit around another body deviates from a perfect circle.
// Circular: e = 0
// Elliptic: 0 < e < 1
// Parabolic: e = 1
// Hyperbolic: e > 1
func calcEccentricityEarthOrbit(t float64) float64 {
	return 0.016708634 - t*(0.000042037+0.0000001267*t)
}

// Return degrees
func calcSunEqOfCenter(t float64) float64 {
	m := calcGeomMeanAnomalySun(t)
	mrad := degToRad(m)
	sinm := math.Sin(mrad)
	sin2m := math.Sin(mrad + mrad)
	sin3m := math.Sin(mrad + mrad + mrad)
	C := sinm*(1.914602-t*(0.004817+0.000014*t)) + sin2m*(0.019993-0.000101*t) + sin3m*0.000289
	return C
}

// Return degrees
func calcSunTrueLong(t float64) float64 {
	l0 := calcGeomMeanLongSun(t)
	c := calcSunEqOfCenter(t)
	O := l0 + c
	return O
}

// Return degrees
func calcSunTrueAnomaly(t float64) float64 {
	m := calcGeomMeanAnomalySun(t)
	c := calcSunEqOfCenter(t)
	v := m + c
	return v
}

// Return degrees
func calcSunApparentLong(t float64) float64 {
	o := calcSunTrueLong(t)
	omega := 125.04 - 1934.136*t
	lambda := o - 0.00569 - 0.00478*math.Sin(degToRad(omega))
	return lambda
}

// Return degrees
func calcMeanObliquityOfEcliptic(t float64) float64 {
	seconds := 21.448 - t*(46.8150+t*(0.00059-t*(0.001813)))
	e0 := 23.0 + (26.0+(seconds/60.0))/60.0
	return e0
}

// Return degrees
func calcObliquityCorrection(t float64) float64 {
	e0 := calcMeanObliquityOfEcliptic(t)
	omega := 125.04 - 1934.136*t
	e := e0 + 0.00256*math.Cos(degToRad(omega))
	return e
}

// Return degrees
func calcSunRtAscension(t float64) float64 {
	e := calcObliquityCorrection(t)
	lambda := calcSunApparentLong(t)
	tananum := (math.Cos(degToRad(e)) * math.Sin(degToRad(lambda)))
	tanadenom := (math.Cos(degToRad(lambda)))
	alpha := radToDeg(math.Atan2(tananum, tanadenom))
	return alpha
}

// Return degrees
func calcSunDeclination(t float64) float64 {
	e := calcObliquityCorrection(t)
	lambda := calcSunApparentLong(t)
	sint := math.Sin(degToRad(e)) * math.Sin(degToRad(lambda))
	theta := radToDeg(math.Asin(sint))
	return theta
}

// Return minutes of time
func calcEquationOfTime(t float64) float64 {
	epsilon := calcObliquityCorrection(t)
	l0 := calcGeomMeanLongSun(t)
	e := calcEccentricityEarthOrbit(t)
	m := calcGeomMeanAnomalySun(t)

	y := math.Tan(degToRad(epsilon) / 2.0)
	y *= y

	sin2l0 := math.Sin(2.0 * degToRad(l0))
	sinm := math.Sin(degToRad(m))
	cos2l0 := math.Cos(2.0 * degToRad(l0))
	sin4l0 := math.Sin(4.0 * degToRad(l0))
	sin2m := math.Sin(2.0 * degToRad(m))

	Etime := y*sin2l0 - 2.0*e*sinm + 4.0*e*y*sinm*cos2l0 - 0.5*y*y*sin4l0 - 1.25*e*e*sin2m
	return radToDeg(Etime) * 4.0
}

// Return radians, for sunset use -HA
func calcHourAngleSunrise(lat float64, solarDec float64) float64 {
	latRad := degToRad(lat)
	sdRad := degToRad(solarDec)
	HAarg := (math.Cos(degToRad(90.833))/(math.Cos(latRad)*math.Cos(sdRad)) - math.Tan(latRad)*math.Tan(sdRad))
	HA := math.Acos(HAarg)
	return HA
}

// Return time in minutes
func calcSunriseSetUTC(rise bool, JD float64, latitude float64, longitude float64) float64 {
	t := calcTimeJulianCent(JD)
	eqTime := calcEquationOfTime(t)
	solarDec := calcSunDeclination(t)
	hourAngle := calcHourAngleSunrise(latitude, solarDec)
	if !rise {
		hourAngle = -hourAngle
	}
	delta := longitude + radToDeg(hourAngle)
	timeUTC := 720.0 - (4.0 * delta) - eqTime
	return timeUTC
}

func calcSunriseSet(rise bool, JD float64, latitude float64, longitude float64, timezone float64, dst bool, unixTimestamp bool) string {

	// These are returned as minutes from midnight
	timeUTC := calcSunriseSetUTC(rise, JD, latitude, longitude)
	timeUTC = calcSunriseSetUTC(rise, JD+timeUTC/1440.0, latitude, longitude)

	timeLocal := timeUTC + (timezone * 60.0)

	if dst == true {
		timeLocal += 60.0
	}

	// 1440.0 minutes are in a day
	if (timeLocal >= 0.0) && (timeLocal < 1440.0) {
		if unixTimestamp == true {
			now := time.Now()
			midnight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			timeLocalSeconds := timeLocal * 60.0
			ts := float64(midnight.Unix()) + timeLocalSeconds
			return fmt.Sprintf("%d", int(ts*1000))
		} else {
			return timeString(timeLocal, 2)
		}
	} else {
		// TODO look into this logic, and apply unixTimestamp logic
		jday := JD
		var increment float64
		if timeLocal < 0.0 {
			increment = 1.0
		} else {
			increment = -1.0
		}
		for (timeLocal < 0.0) || (timeLocal >= 1440.0) {
			timeLocal += increment * 1440.0
			jday -= increment
		}
		return timeDateString(jday, timeLocal)
	}
}

func timeDateString(jd float64, minutes float64) string {
	text := timeString(jd, 2)
	text += " "
	text += dayString(jd, 0, 2)
	return text
}

func timeString(minutes float64, flag int) string {
	if minutes >= 0.0 && minutes < 1440.0 {
		floatHour := minutes / 60.0
		hour := math.Floor(floatHour)

		floatMinute := 60.0 * (floatHour - hour)
		minute := math.Floor(floatMinute)

		floatSecond := 60.0 * (floatMinute - minute)
		second := math.Floor(floatSecond)

		if second > 59.0 {
			second = 0.0
			minute += 1.0
		}

		if flag > 2 && second >= 30.0 {
			minute += 1.0
		}

		if minute > 59.0 {
			minute = 0.0
			hour += 2.0
		}

		var ampm string
		if hour > 12.0 {
			ampm = "PM"
			hour -= 12.0
		} else {
			ampm = "AM"
		}

		output := zeroPad(int(hour))
		output += ":"
		output += zeroPad(int(minute))
		output += ":"
		output += zeroPad(int(second))
		output += " "
		output += ampm

		return output
	} else {
		output := "error"

		return output
	}
}

func dayString(jd float64, next int, flag int) string {
	if jd < 900000.0 || jd > 2817000.0 {
		return "error"
	} else {
		z := math.Floor(jd + 0.5)
		f := (jd + 0.5) - z
		var A float64
		var alpha float64
		if z < 2299161.0 {
			A = z
		} else {
			alpha = math.Floor((z - 1867216.25) / 36524.25)
			A = z + 1.0 + alpha - math.Floor(alpha/4.0)
		}
		B := A + 1524.0
		C := math.Floor((B - 122.1) / 365.25)
		D := math.Floor(365.25 * C)
		E := math.Floor((B - D) / 30.6001)
		day := B - D - math.Floor(30.6001*E) + f
		var month int
		if E < 14.0 {
			month = int(E) - 1
		} else {
			month = int(E) - 13
		}
		var year int
		if month > 2 {
			year = int(C) - 4716
		} else {
			year = int(C) - 4715
		}

		var nextText string
		if next > 0 {
			nextText = "next"
		} else {
			nextText = "prev"
		}

		output := strconv.Itoa(int(month))
		output += "-"
		output += strconv.Itoa(int(day))
		output += "-"
		output += strconv.Itoa(int(year))
		output += " "
		output += nextText

		return output
	}
}

// This will always be a little off due to leap seconds
func getJulianFromUnixTimestamp(ts float64) float64 {
	return (ts / 86400.0) + 2440587.5
}

func zeroPad(n int) string {
	nS := strconv.Itoa(n)
	nSnum := utf8.RuneCountInString(nS)
	if nSnum < 2 {
		nS = "0" + nS
	}
	return nS
}
